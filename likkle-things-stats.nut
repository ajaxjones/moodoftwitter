// Likkle things status board  with remote control and keep alive
 
// Variable to represent LED state
ledState <- 0;

// Variable to represent LED inhibit state
inhibit <- 0;

// Set the Output port to send Alive variable
local output = OutputPort("alive", "number");


function watchdog() {
    imp.wakeup(5*60, watchdog);
    server.log("watchdog");

    // Output to send I am alive message
    local temp = date();
    local itime = time();
    output.set(itime);   
    server.show("I am alive "+itime);
}
 
function LEDcontrol(whichone,state){
    server.log("we got a state change "+ whichone + " " + state);
    // server.log(typeof(state));
    
    local LEDoption = state.tointeger();
    
    switch(whichone) {
        case "9": 
            {
                hardware.pin9.write(LEDoption);    
                break;
          }
        case "8": 
            {
                hardware.pin8.write(LEDoption);    
                break;
          }
        case "7": 
            {
                hardware.pin7.write(LEDoption);    
                break;
          }
        case "5": 
            {
                hardware.pin5.write(LEDoption);    
                break;
          }
        case "2": 
            {
                hardware.pin2.write(LEDoption);    
                break;
          }
        case "1": 
            {
                hardware.pin1.write(LEDoption);    
                break;
          }

    }
    
  }

// blink function called every 100ms
function blink()
{
    
}
 
// input class for LED control channel
class input extends InputPort
{
  name = "LED control"
  type = "number"
  firstposty = null;
 
  function set(value)
  {
      server.log("We have a HTTP input = "+ value);
      
      local firstposty = value.tostring();
      
      local oneposty = firstposty.slice(0,1);
      oneposty.tostring();
      
      local twoposty = firstposty.slice(1,2);
      local ledOption = twoposty.tostring();
      server.log("Input split = " + oneposty + " " + ledOption)
      
      switch(oneposty) {
          case "9": 
          {
              LEDcontrol(oneposty,ledOption);
              break;
          }
          case "8":
          {
              LEDcontrol(oneposty,ledOption);
              break;
          }
          case "7":
          {
              LEDcontrol(oneposty,ledOption);
              break;
          }
          case "5":
          {
              LEDcontrol(oneposty,ledOption);
              break;
          }
          case "2":
          {
              LEDcontrol(oneposty,ledOption);
              break;
          }
          case "1":
          {
              LEDcontrol(oneposty,ledOption);
              break;
          }
          default:
            server.log("OOPs didnt understand that one");
            break;
      }      
  }
}
 
// Configure pin 1 as an open drain output with internal pull up
hardware.pin9.configure(DIGITAL_OUT_OD_PULLUP);
hardware.pin8.configure(DIGITAL_OUT_OD_PULLUP);
hardware.pin7.configure(DIGITAL_OUT_OD_PULLUP);
hardware.pin5.configure(DIGITAL_OUT_OD_PULLUP);
hardware.pin2.configure(DIGITAL_OUT_OD_PULLUP);
hardware.pin1.configure(DIGITAL_OUT_OD_PULLUP);
 
// Register with the server
imp.configure("likkle-things-stats", [input()], [output]);

// set out our plan
server.log("LIKKLE dual slice of local CASE string posty and state no blink str bank HTTP SEND= ");
 
// Start Watchdog
watchdog();

// End of code.
